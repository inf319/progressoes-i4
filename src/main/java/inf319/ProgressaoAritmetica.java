package inf319;

public class ProgressaoAritmetica extends Progressao {

	private long incremento;

	public ProgressaoAritmetica() {
		this(1);
	}

	public ProgressaoAritmetica(int incremento) {
		this.incremento = incremento;
		cache = new GerenteProgressao<Long>();
		inicia();
	}

	public long inicia() {
		valCor = 0;
		return addToCache();
	}

	public long proxTermo() {
		valCor += incremento;
		return addToCache();
	}

}
