package inf319;

public abstract class Progressao {

    protected long valCor;
    protected GerenteProgressao<Long> cache;

    public abstract long inicia();

    public abstract long proxTermo();

    public long iesimoTermo(int i) {
    	if (cache.contains(i)) {
			return cache.get(i);
		}
    	
        long iesimo = inicia();
        for (int j = 0; j < i; j++) {
            iesimo = proxTermo();
        }
        return iesimo;
    }

    public String imprimeProgressao(int n) {
        StringBuilder progressao = new StringBuilder();
        progressao.append(inicia());
        for (int j = 0; j < n; j++) {
            progressao.append(' ');
            progressao.append(proxTermo());
        }
        progressao.append('\n');
        return progressao.toString();
    }
    
    public Long addToCache() {
    	if (cache.contains((int) valCor)) {
			return cache.get((int) valCor);
		}
		cache.add(valCor, valCor);
		return valCor;
    }

}
