package inf319;

import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("hiding")
public class GerenteProgressao<Long> {
	private Map<Long, Long> objects;
	
	public GerenteProgressao() {
		objects = new HashMap<Long, Long>();
	}
	
	public Long get(int key) {
		return this.objects.get(key);
	}
	
	public void add(Long key, Long value) {
		this.objects.put(key, value);
	}
	
	public boolean contains(int key) {
		return this.objects.containsKey(key);
	}
}
