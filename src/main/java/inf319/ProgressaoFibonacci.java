package inf319;

public class ProgressaoFibonacci extends Progressao {

	private long valPrev;

	public ProgressaoFibonacci() {
		cache = new GerenteProgressao<Long>();
		inicia();
	}

	public long inicia() {
		valCor = 0;
		valPrev = 1;
		return addToCache();
	}

	public long proxTermo() {
		valCor += valPrev;
		valPrev = valCor - valPrev;
		return addToCache();
	}
}
